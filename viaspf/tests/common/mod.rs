use async_trait::async_trait;
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};
use viaspf::lookup::{Lookup, LookupError, LookupResult, Name};

// Closures passed to `MockLookupBuilder` are easiest to read and write when
// using a simple `match` with string literals as the match expressions. Use
// lower-case ASCII in strings and always include the trailing dot!
#[derive(Default)]
pub struct MockLookupBuilder {
    lookup_a: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<Ipv4Addr>> + Send + Sync + 'static>>,
    lookup_aaaa: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<Ipv6Addr>> + Send + Sync + 'static>>,
    lookup_mx: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<Name>> + Send + Sync + 'static>>,
    lookup_txt: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<String>> + Send + Sync + 'static>>,
    lookup_ptr: Option<Box<dyn Fn(IpAddr) -> LookupResult<Vec<Name>> + Send + Sync + 'static>>,
}

impl MockLookupBuilder {
    pub fn lookup_a(
        mut self,
        value: impl Fn(&Name) -> LookupResult<Vec<Ipv4Addr>> + Send + Sync + 'static,
    ) -> Self {
        self.lookup_a = Some(Box::new(value));
        self
    }

    pub fn lookup_aaaa(
        mut self,
        value: impl Fn(&Name) -> LookupResult<Vec<Ipv6Addr>> + Send + Sync + 'static,
    ) -> Self {
        self.lookup_aaaa = Some(Box::new(value));
        self
    }

    pub fn lookup_mx(
        mut self,
        value: impl Fn(&Name) -> LookupResult<Vec<Name>> + Send + Sync + 'static,
    ) -> Self {
        self.lookup_mx = Some(Box::new(value));
        self
    }

    pub fn lookup_txt(
        mut self,
        value: impl Fn(&Name) -> LookupResult<Vec<String>> + Send + Sync + 'static,
    ) -> Self {
        self.lookup_txt = Some(Box::new(value));
        self
    }

    pub fn lookup_ptr(
        mut self,
        value: impl Fn(IpAddr) -> LookupResult<Vec<Name>> + Send + Sync + 'static,
    ) -> Self {
        self.lookup_ptr = Some(Box::new(value));
        self
    }

    pub fn build(self) -> MockLookup {
        MockLookup {
            lookup_a: self.lookup_a,
            lookup_aaaa: self.lookup_aaaa,
            lookup_mx: self.lookup_mx,
            lookup_txt: self.lookup_txt,
            lookup_ptr: self.lookup_ptr,
        }
    }
}

// Note that `Name` inputs are converted to lower-case ASCII to simulate
// case-insensitive lookups and make matching easier.
#[derive(Default)]
pub struct MockLookup {
    lookup_a: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<Ipv4Addr>> + Send + Sync + 'static>>,
    lookup_aaaa: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<Ipv6Addr>> + Send + Sync + 'static>>,
    lookup_mx: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<Name>> + Send + Sync + 'static>>,
    lookup_txt: Option<Box<dyn Fn(&Name) -> LookupResult<Vec<String>> + Send + Sync + 'static>>,
    lookup_ptr: Option<Box<dyn Fn(IpAddr) -> LookupResult<Vec<Name>> + Send + Sync + 'static>>,
}

impl MockLookup {
    pub fn builder() -> MockLookupBuilder {
        Default::default()
    }
}

#[async_trait]
impl Lookup for MockLookup {
    async fn lookup_a(&self, name: &Name) -> LookupResult<Vec<Ipv4Addr>> {
        self.lookup_a
            .as_ref()
            .map_or(Err(LookupError::NoRecords), |f| f(&to_lowercase(name)))
    }

    async fn lookup_aaaa(&self, name: &Name) -> LookupResult<Vec<Ipv6Addr>> {
        self.lookup_aaaa
            .as_ref()
            .map_or(Err(LookupError::NoRecords), |f| f(&to_lowercase(name)))
    }

    async fn lookup_mx(&self, name: &Name) -> LookupResult<Vec<Name>> {
        self.lookup_mx
            .as_ref()
            .map_or(Err(LookupError::NoRecords), |f| f(&to_lowercase(name)))
    }

    async fn lookup_txt(&self, name: &Name) -> LookupResult<Vec<String>> {
        self.lookup_txt
            .as_ref()
            .map_or(Err(LookupError::NoRecords), |f| f(&to_lowercase(name)))
    }

    async fn lookup_ptr(&self, ip: IpAddr) -> LookupResult<Vec<Name>> {
        self.lookup_ptr
            .as_ref()
            .map_or(Err(LookupError::NoRecords), |f| f(ip))
    }
}

fn to_lowercase(name: &Name) -> Name {
    Name::new(&name.as_str().to_ascii_lowercase()).unwrap()
}
